# micropython_unittest_junit

This package provides a micropython unittest testRunner which can be used to run unit tests in a project and output the results as a JUnit xml file for automated parsing and reporting.

It's intended to be used in conjunction with https://github.com/micropython/micropython-lib/tree/master/python-stdlib/unittest 0.9 or newer.

It can be used in a unit test file as such:

```
import unittest
from unittest_junit import JunitTestRunner


class MyTest(unittest.TestCase):
    def test_something(self):
        assert True


if __name__ == "__main__":
    unittest.main(testRunner=JunitTestRunner)

```

It can also be run as a command line utility to run all unit tests from the current working directory file tree:

```
    $ cd /path/to/app/src
    $ micropython -m unittest_junit

```
