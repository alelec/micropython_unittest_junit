#!/usr/bin/env python
# -*- coding: UTF-8 -*-
# unittest_junit is a micropython unittest.TestRunner which outputs JUnit report format XML files.
# It can be imported and passed into unittest, eg.
#    import unittest
#    import unittest_junit
#    ...
#    if __name__ == '__main__':
#        unittest.main(testRunner=unittest_junit.JunitTestRunner)
#
# Alternatively it can be run directly as a module, in which case all arguments are passed directly to unittest module

import os
import sys
import time
import xml.etree.ElementTree as ET
import unittest
from unittest import TestResult

maxunicode = 65535
localtime_keys = ("year", "month", "day", "hour", "minute", "second", "weekday", "yearday")
ISO8601EXT_FMT = "{year}-{month:02}-{day:02}T{hour:02}:{minute:02}:{second:02}"

# Example xml output: based on the understanding of what Jenkins can parse for JUnit XML files.
"""
<?xml version="1.0" encoding="utf-8"?>
<testsuites errors="1" failures="1" tests="4" time="45">
    <testsuite errors="1" failures="1" hostname="localhost" id="0" name="test1"
               package="testdb" tests="4" timestamp="2012-11-15T01:02:29">
        <properties>
            <property name="assert-passed" value="1"/>
        </properties>
        <testcase classname="testdb.directory" name="1-passed-test" time="10"/>
        <testcase classname="testdb.directory" name="2-failed-test" time="20">
            <failure message="Assertion FAILED: failed assert" type="failure">
                the output of the testcase
            </failure>
        </testcase>
        <testcase classname="package.directory" name="3-errord-test" time="15">
            <error message="Assertion ERROR: error assert" type="error">
                the output of the testcase
            </error>
        </testcase>
        <testcase classname="package.directory" name="3-skipped-test" time="0">
            <skipped message="SKIPPED Test" type="skipped">
                the output of the testcase
            </skipped>
        </testcase>
        <testcase classname="testdb.directory" name="3-passed-test" time="10">
            <system-out>
                I am system output
            </system-out>
            <system-err>
                I am the error output
            </system-err>
        </testcase>
    </testsuite>
</testsuites>
"""

def decode(var, encoding):
    """
    If not already unicode, decode it.
    """
    ret = var.decode() if isinstance(var, bytes) else str(var)
    return ret


class JunitTestRunner(unittest.TestRunner):
    def __init__(self, outputDir=None) -> None:
        super().__init__()
        self.suite: unittest.TestSuite = None
        self.outputDir = outputDir or "results"
        self.timestamp = ''
        self.elapsed_sec = 0

    def run(self, suite: unittest.TestSuite):
        self.suite = suite
        start_time = time.ticks_ms()
        datetime = dict(zip(localtime_keys, time.localtime()))
        self.timestamp = ISO8601EXT_FMT.format(**datetime)
        # Run the test suite
        res: unittest.TestResult = super().run(suite)

        self.elapsed_sec = "%0.2f" % (time.ticks_diff(time.ticks_ms(), start_time) / 1000)
        try:
            os.mkdir(self.outputDir)
        except OSError:
            pass
        filename = suite.name
        if filename == "__main__":
            import __main__
            filename = __main__.__file__.split("/")[-1]
            if filename.endswith(".py"):
                filename = filename[:-3]
        xml_name = os.sep.join((self.outputDir, "%s.xml" % filename))
        with open(xml_name, "wb") as xmlfile:
            try:
                cd = os.getcwd()
            except AttributeError:
                cd = "."
            print("writing xml to %s/%s" % (cd, xml_name))
            self.to_xml_report_file(xmlfile, res)

        return res

    def build_xml_doc(self, test_result: TestResult, encoding=None):
        """
        Builds the XML document for the JUnit test suite.
        Produces clean unicode strings and decodes non-unicode with the help of encoding.
        @param encoding: Used to decode encoded strings.
        @return: XML document with unicode string elements
        """

        # build the test suite element
        test_suite_attributes = dict()
        # if any(c.assertions for c in self.test_cases):
        #     test_suite_attributes["assertions"] = str(sum([int(c.assertions) for c in self.test_cases if c.assertions]))
        # test_suite_attributes["disabled"] = str(len([c for c in self.test_cases if not c.is_enabled]))
        test_suite_attributes["errors"] = str(test_result.errorsNum)
        test_suite_attributes["failures"] = str(test_result.failuresNum)
        test_suite_attributes["name"] = decode(self.suite.name, encoding)
        test_suite_attributes["skipped"] = str(test_result.skippedNum)
        test_suite_attributes["tests"] = str(test_result.testsRun)
        test_suite_attributes["time"] = self.elapsed_sec

        test_suite_attributes["hostname"] = decode(sys.implementation, encoding)
        # if self.id:
        #     test_suite_attributes["id"] = decode(self.id, encoding)
        # if self.package:
        #     test_suite_attributes["package"] = decode(self.package, encoding)
        # if self.timestamp:
        test_suite_attributes["timestamp"] = self.timestamp
        # if self.file:
        #     test_suite_attributes["file"] = decode(self.file, encoding)
        # if self.log:
        #     test_suite_attributes["log"] = decode(self.log, encoding)
        # if self.url:
        #     test_suite_attributes["url"] = decode(self.url, encoding)

        xml_element = ET.Element("testsuite", test_suite_attributes)

        # add any properties
        # if self.properties:
        #     props_element = ET.SubElement(xml_element, "properties")
        #     for k, v in self.properties.items():
        #         attrs = {"name": decode(k, encoding), "value": decode(v, encoding)}
        #         ET.SubElement(props_element, "property", attrs)

        # # add test suite stdout
        # if self.stdout:
        #     stdout_element = ET.SubElement(xml_element, "system-out")
        #     stdout_element.text = decode(self.stdout, encoding)

        # # add test suite stderr
        # if self.stderr:
        #     stderr_element = ET.SubElement(xml_element, "system-err")
        #     stderr_element.text = decode(self.stderr, encoding)

        # test cases
        # for case in self.suit.test_cases:
        test_case_attributes = dict()
        test_case_attributes["name"] = decode(self.suite.name, encoding)
        # test_result.assertions:
        #     # Number of assertions in the test case
        #     test_case_attributes["assertions"] = "%d" % test_result.assertions
        if self.elapsed_sec:
            test_case_attributes["time"] = self.elapsed_sec
        if self.timestamp:
            test_case_attributes["timestamp"] = self.timestamp
        if self.suite.name:
            test_case_attributes["classname"] = decode(self.suite.name.split(".")[-1], encoding)
        # if test_result.status:
        #     test_case_attributes["status"] = decode(test_result.status, encoding)
        # if test_result.category:
        #     test_case_attributes["class"] = decode(test_result.category, encoding)
        # if test_result.file:
        #     test_case_attributes["file"] = decode(test_result.file, encoding)
        # if test_result.line:
        #     test_case_attributes["line"] = decode(test_result.line, encoding)
        # if test_result.log:
        #     test_case_attributes["log"] = decode(test_result.log, encoding)
        # if test_result.url:
        #     test_case_attributes["url"] = decode(test_result.url, encoding)

        test_case_element = ET.SubElement(xml_element, "testcase", test_case_attributes)

        # failures
        for test_detail, traceback in test_result.failures:
            detail = " ".join((str(i) for i in test_detail))
            attrs = {"type": "failure"}
            attrs["message"] = detail
            failure_element = ET.Element("failure", attrs)
            failure_element.text = decode(traceback, encoding)
            test_case_element.append(failure_element)

        # errors
        for test_detail, traceback in test_result.errors:
            detail = " ".join((str(i) for i in test_detail))
            attrs = {"type": "error"}
            attrs["message"] = detail
            error_element = ET.Element("error", attrs)
            error_element.text = decode(traceback, encoding)
            test_case_element.append(error_element)

        # skippeds
        for name, test_detail, reason in test_result.skipped:
            attrs = {"type": "skipped"}
            if reason:
                attrs["message"] = decode(reason, encoding)
            skipped_element = ET.Element("skipped", attrs)
            # if skipped["output"]:
            #     skipped_element.text = decode(skipped["output"], encoding)
            test_case_element.append(skipped_element)

        # # test stdout
        # if test_result.stdout:
        #     stdout_element = ET.Element("system-out")
        #     stdout_element.text = decode(test_result.stdout, encoding)
        #     test_case_element.append(stdout_element)

        # # test stderr
        # if test_result.stderr:
        #     stderr_element = ET.Element("system-err")
        #     stderr_element.text = decode(test_result.stderr, encoding)
        #     test_case_element.append(stderr_element)

        return xml_element

    def to_xml_report_string(self, test_result: TestResult, prettyprint=True, encoding=None):
        """
        Returns the string representation of the JUnit XML document.
        @param encoding: The encoding of the input.
        @return: unicode string
        """

        xml_element = ET.Element("testsuites")
        attributes = defaultdict(int)
        tr_xml = self.build_xml_doc(test_result, encoding=encoding)
        for key in ["disabled", "errors", "failures", "tests"]:
            attributes[key] += int(tr_xml.get(key, 0))
        for key in ["time"]:
            attributes[key] += float(tr_xml.get(key, 0))
        xml_element.append(tr_xml)
        for key, value in attributes.items():
            xml_element.set(key, str(value))

        xml_string = ET.tostring(xml_element, encoding=encoding)
        # is encoded now
        xml_string = _clean_illegal_xml_chars(xml_string.decode(encoding or "utf-8"))
        # is unicode now

        ## minidom not available on micropython
        # if prettyprint:
        #     # minidom.parseString() works just on correctly encoded binary strings
        #     xml_string = xml_string.encode(encoding or "utf-8")
        #     xml_string = xml.dom.minidom.parseString(xml_string)
        #     # toprettyxml() produces unicode if no encoding is being passed or binary string with an encoding
        #     xml_string = xml_string.toprettyxml(encoding=encoding)
        #     if encoding:
        #         xml_string = xml_string.decode(encoding)
        #     # is unicode now
        return xml_string


    def to_xml_report_file(self, file_descriptor, test_result: TestResult, prettyprint=True, encoding=None):
        """
        Writes the JUnit XML document to a file.
        """
        xml_string = self.to_xml_report_string(test_result, prettyprint=prettyprint, encoding=encoding)
        # has problems with encoded str with non-ASCII (non-default-encoding) characters!
        file_descriptor.write(xml_string)


def _clean_illegal_xml_chars(string_to_clean):
    """
    Removes any illegal unicode characters from the given XML string.

    @see: http://stackoverflow.com/questions/1707890/fast-way-to-filter-illegal-xml-unicode-chars-in-python
    """

    illegal_unichrs = [
        [0x0000, 0x0008],
        [0x000B, 0x001F],
        [0x007F, 0x0084],
        [0x0086, 0x009F],
        [0xD800, 0xDFFF],
        [0xFDD0, 0xFDDF],
        [0xFFFE, 0xFFFF],
        [0x0001FFFE, 0x0001FFFF],
        [0x0002FFFE, 0x0002FFFF],
        [0x0003FFFE, 0x0003FFFF],
        [0x0004FFFE, 0x0004FFFF],
        [0x0005FFFE, 0x0005FFFF],
        [0x0006FFFE, 0x0006FFFF],
        [0x0007FFFE, 0x0007FFFF],
        [0x0008FFFE, 0x0008FFFF],
        [0x0009FFFE, 0x0009FFFF],
        [0x000AFFFE, 0x000AFFFF],
        [0x000BFFFE, 0x000BFFFF],
        [0x000CFFFE, 0x000CFFFF],
        [0x000DFFFE, 0x000DFFFF],
        [0x000EFFFE, 0x000EFFFF],
        [0x000FFFFE, 0x000FFFFF],
        [0x0010FFFE, 0x0010FFFF],
    ]

    invalid = []
    for i, c in enumerate(string_to_clean):
        o = ord(c)
        for start, end in illegal_unichrs:
            if start <= o <= end:
                invalid.append(i)
                break
    for i in reversed(invalid):
        string_to_clean = string_to_clean[0:i] + string_to_clean[i+1:]

    return string_to_clean


class defaultdict:
    @staticmethod
    def __new__(cls, default_factory=None, **kwargs):
        # Some code (e.g. urllib.urlparse) expects that basic defaultdict
        # functionality will be available to subclasses without them
        # calling __init__().
        self = super(defaultdict, cls).__new__(cls)
        self.d = {}
        return self

    def __init__(self, default_factory=None, **kwargs):
        self.d = kwargs
        self.default_factory = default_factory

    def __getitem__(self, key):
        try:
            return self.d[key]
        except KeyError:
            v = self.__missing__(key)
            self.d[key] = v
            return v

    def __setitem__(self, key, v):
        self.d[key] = v

    def __delitem__(self, key):
        del self.d[key]

    def __contains__(self, key):
        return key in self.d

    def __missing__(self, key):
        if self.default_factory is None:
            raise KeyError(key)
        return self.default_factory()

    def items(self):
        for key in self.d:
            yield key, self.d[key]


def main(*args, **kwargs):
    if "testRunner" not in kwargs:
        kwargs["testRunner"] = JunitTestRunner
    return unittest.main(*args, **kwargs)


if __name__ == "__main__":
    unittest.TestRunner = JunitTestRunner
    # unittest.__main__ is provided by unittest-discover
    sys.argv[0] = "unittest"
    import unittest.__main__
